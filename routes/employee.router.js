const express = require('express')
const router = express.Router()
const employeeController = require('../controllers/employee.controller');

router.route("/").post(employeeController.createEmployee).get(employeeController.getEmployees);

router
  .route("/:employeeId")
  .get(employeeController.getEmployee)
  .put(employeeController.updateEmployee)
  .delete(employeeController.deleteEmployee);



module.exports = router







/**
 * @swagger
 * tags:
 *   name: Employees
 *   description: Employee management and retrieval
 */

/**
 * @swagger
 *  /employees:
 *    post:
 *      summary: Create a employee
 *      description: Only admins can create other employees.
 *      tags: [Employees]
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - firstName
 *                - lastName
 *                - dob
 *                - email
 *                - address
 *                - phoneNumber
 *                - desigNation
 *                - city
 *                - gender
 *                - salary
 *              properties:
 *                firstName:
 *                  type: string
 *                lastName:
 *                  type: string
 *                dob:
 *                  type: Date
 *                email:
 *                   type: string
 *                address: 
 *                   type: string
 *                phoneNumber:
 *                   type: string
 *                desigNation:
 *                   type: string
 *                city:
 *                   type: string
 *                gender:
 *                   type: string
 *                salary:
 *                    type string
 *              example:
 *                firstName: munusamy
 *                lastName: s
 *                dob: 20-20-2000
 *                email: munu@example.com
 *                address: 1/71,tp
 *                phoneNumber: "9876543210"
 *                desigNation: developer
 *                city: tirupur
 *                gender: male
 *                salary: 10000
 *      responses:
 *        "201":
 *          description: Created
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  employee:
 *                    $ref: '#/components/schemas/Employee'
 *        "400":
 *          $ref: '#/components/responses/DuplicateEmail'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *
 *    get:
 *      summary: Get all employees
 *      description: Only admins can retrieve all employees.
 *      tags: [Employees]
 *      parameters:
 *        - in: query
 *          name: firstName
 *          schema:
 *            type: string
 *          description: firstName
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  results:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Employee'
 *                  page:
 *                    type: integer
 *                    example: 1
 *                  limit:
 *                    type: integer
 *                    example: 10
 *                  totalPages:
 *                    type: integer
 *                    example: 1
 *                  totalResults:
 *                    type: integer
 *                    example: 1
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 */

/**
 * @swagger
 *  /employees/{id}:
 *    get:
 *      summary: Get a employee
 *      description: Only admins can fetch other employees.
 *      tags: [Employees]
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: string
 *          description: Employee id
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/Employee'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 *
 *    put:
 *      summary: Update a employee
 *      description: Only admins can update other employees.
 *      tags: [Employees]
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: string
 *          description: Employee id
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - firstName
 *                - lastName
 *                - dob
 *                - email
 *                - address
 *                - phoneNumber
 *                - desigNation
 *                - city
 *                - gender
 *                - salary
 *              properties:
 *                firstName:
 *                  type: string
 *                lastName:
 *                  type: string
 *                dob:
 *                  type: Date
 *                email:
 *                   type: string
 *                address: 
 *                   type: string
 *                phoneNumber:
 *                   type: string
 *                desigNation:
 *                   type: string
 *                city:
 *                   type: string
 *                gender:
 *                   type: string
 *                salary:
 *                    type string
 *              example:
 *                firstName: munusamy
 *                lastName: s
 *                dob: 20-20-2000
 *                email: munu@example.com
 *                address: 1/71,tp
 *                phoneNumber: "9876543210"
 *                desigNation: developer
 *                city: tirupur
 *                gender: male
 *                salary: 10000
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/Employee'
 *        "400":
 *          $ref: '#/components/responses/DuplicateEmail'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 *
 *    delete:
 *      summary: Delete a employee
 *      description: Only admins can delete other employees.
 *      tags: [Employees]
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: string
 *          description: Employee id
 *      responses:
 *        "200":
 *          description: No content
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 */


