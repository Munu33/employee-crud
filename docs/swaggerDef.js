
const { version } = require('../package.json');

const swaggerDef = {
  openapi: '3.0.2',
  info: {
    title: "Employee API",
    description: "Employee API Information",
    version,
  },
  servers: [
    {
      url: 'http://localhost:7000', // change url based on (local/production)
    },
    
  ],
};

module.exports = swaggerDef;