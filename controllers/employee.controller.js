const Employee = require('../models/employee.model');
const path = require('path')


const createEmployee = (async(req,res) => {
    //console.log(req.body)
    const employee = new Employee({
        firstName:req.body.firstName,
        lastName:req.body.lastName,
        dob:req.body.dob,
        email:req.body.email,
        address:req.body.address,
        phoneNumber:req.body.phoneNumber,
        desigNation:req.body.desigNation,
        city:req.body.city,
        gender:req.body.gender,
        salary:req.body.salary,
    })

    try{
        const e1 =  await Employee.create(req.body)
        res.send(e1) //using JSON for Send
    }catch(err){
        res.send(err)
    }
})


const getEmployees = (async(req,res) => {
    console.log(req,"employee")
    try{
           const employees = await Employee.find({isDeleted:false})
           res.json(employees)
        //res.sendFile(path.join(__dirname ,"../index.html"))

    }catch(err){
        res.send('Error ')
        
    }
})

const getEmployee = (async(req,res) => {
    try{
           const employee = await Employee.findById(req.params.employeeId)
           res.json(employee)
    }catch(err){
        res.send('Error ' + err)
    }
   /* res.send('id: ' + req.query.id);*/
    /*res.json(employee)*/
})





const updateEmployee = (async(req,res)=> {
    try{
        console.log(req.body,req.params.employeeId)
        const employee = await Employee.findByIdAndUpdate(req.params.employeeId,req.body)
       
        /*const u1 = await employee.save()*/
        res.send(employee)   
    }catch(err){
        res.send(err)
    }

})

const deleteEmployee = (async(req,res)=> {
    try{
        console.log('Passed')
        const employee = await Employee.findByIdAndUpdate(req.params.employeeId,{isDeleted:true})
        //employee.sub = req.body.sub
        //const e1 = await employee.save()
           res.json(employee)
    }catch(err){
        res.send('Error')
    }

})

  module.exports = {
    createEmployee,
    getEmployees,
    getEmployee,
    updateEmployee,
    deleteEmployee
  };