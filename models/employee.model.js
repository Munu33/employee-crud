const mongoose = require('mongoose')


const employeeSchema = new mongoose.Schema({
 

  firstName:{
    type:String 
  },
  lastName:{
    type:String 
  },
  dob: {
    type: String
   },
  email:{
    type:String,
    required:true
  }, 
  address:{
    type:String
  },
  phoneNumber:{
    type:Number
  },
  desigNation:{
    type:String
  },
  city:{
    type:String
  },
  gender:{
    type:String
  },
  salary:{
    type:String
  },
  
  isDeleted:{
  type:Boolean,
  default:false
}




 
});

module.exports = mongoose.model('Employee',employeeSchema)