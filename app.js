const express = require('express')
const mongoose = require('mongoose')
const employeeRouter = require('./routes/employee.router');
const docsRouter = require('./routes/docs.route');
const mongodburl = require("./config/index.json")
const url = mongodburl.mongoose.url
//const url = 'mongodb://localhost/employeeDB'

const app = express()

mongoose.connect(url, {useNewUrlParser:true,useUnifiedTopology: true })
const con = mongoose.connection

con.on('open', () => {
    console.log('connected...')
})

app.use(express.json())


app.use('/employees',employeeRouter)
app.use('/docs',docsRouter)


let port = process.env.PORT || 7000

app.listen(port, () => {
    console.log('Server started')
})